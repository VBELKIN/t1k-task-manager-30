package ru.t1k.vbelkin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1k.vbelkin.tm.api.repository.ICommandRepository;
import ru.t1k.vbelkin.tm.api.repository.IProjectRepository;
import ru.t1k.vbelkin.tm.api.repository.ITaskRepository;
import ru.t1k.vbelkin.tm.api.repository.IUserRepository;
import ru.t1k.vbelkin.tm.api.service.*;
import ru.t1k.vbelkin.tm.command.AbstractCommand;
import ru.t1k.vbelkin.tm.enumerated.Role;
import ru.t1k.vbelkin.tm.enumerated.Status;
import ru.t1k.vbelkin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1k.vbelkin.tm.exception.system.CommandNotSupportedException;
import ru.t1k.vbelkin.tm.model.Project;
import ru.t1k.vbelkin.tm.repository.CommandRepository;
import ru.t1k.vbelkin.tm.repository.ProjectRepository;
import ru.t1k.vbelkin.tm.repository.TaskRepository;
import ru.t1k.vbelkin.tm.repository.UserRepository;
import ru.t1k.vbelkin.tm.service.*;
import ru.t1k.vbelkin.tm.util.SystemUtil;
import ru.t1k.vbelkin.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@Getter
public final class Bootstrap implements IServiceLocator {

    private static final String PACKAGE_COMMANDS = "ru.t1k.vbelkin.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository(propertyService);

    @NotNull
    private final IUserService userService = new UserService(userRepository, projectService, taskService, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@Nullable final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void prepareStartup() {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        backup.stop();
        fileScanner.stop();
    }


    private void initDemoData() {
        userService.create("test", "test", "test@test.ru");
        userService.create("user", "user", "user@user.ru");
        userService.create("admin", "admin", Role.ADMIN);

        projectService.add(new Project("Test Project", Status.IN_PROGRESS));
        projectService.add(new Project("Demo Project", Status.NOT_STARTED));
        projectService.add(new Project("Beta Project", Status.IN_PROGRESS));
        projectService.add(new Project("Alpha Project", Status.COMPLETED));

        //taskService.create("MEGA TASK");
        //taskService.create("BETA TASK");
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command, boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void exit() {
        System.exit(0);
    }

    private boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        @Nullable final String arg = args[0];
        try {
            processArgument(arg);
        } catch (@NotNull final Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }
        return true;
    }

    public void run(@Nullable final String[] args) {
        if (processArguments(args)) System.exit(0);
        prepareStartup();
        processCommands();
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("Enter command:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void processCommand(String command) {
        processCommand(command, true);
    }

}
