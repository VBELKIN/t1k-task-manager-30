package ru.t1k.vbelkin.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1k.vbelkin.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class FileScanner {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();
    @NotNull
    private final List<String> commands = new ArrayList<>();
    @NotNull
    private final File folder = new File("./");
    @NotNull
    private Bootstrap bootstrap;

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    private void init() {
        @NotNull final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommandsWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));
        es.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    private void process() {
        for (@NotNull File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    file.delete();
                    bootstrap.processCommand(fileName);
                } catch (@NotNull final Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void stop() {
        es.shutdown();
    }

    public void start() {
        init();
    }

}
