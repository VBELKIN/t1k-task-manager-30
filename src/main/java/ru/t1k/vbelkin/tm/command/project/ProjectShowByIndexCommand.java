package ru.t1k.vbelkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1k.vbelkin.tm.model.Project;
import ru.t1k.vbelkin.tm.util.TerminalUtil;

public class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-show-by-index";

    @NotNull
    public static final String DESCRIPTION = "Display project by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();
        @NotNull final Project project = getProjectService().findOneByIndex(userId, index);
        showProject(project);
    }

}
