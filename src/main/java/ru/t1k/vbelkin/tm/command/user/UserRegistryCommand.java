package ru.t1k.vbelkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1k.vbelkin.tm.api.service.IAuthService;
import ru.t1k.vbelkin.tm.enumerated.Role;
import ru.t1k.vbelkin.tm.model.User;
import ru.t1k.vbelkin.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-registry";

    @NotNull
    private static final String DESCRIPTION = "registry user";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.print("ENTER EMAIL: ");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        @NotNull final User user = authService.registry(login, password, email);
        showUser(user);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
